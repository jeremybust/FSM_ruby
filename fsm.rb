require 'state_machine'
require 'highline/import'
class Popvehicle
  state_machine :initial => :parked do

      event :park do
        transition [:idling] => :parked
      end

      event :ignite do
        transition [:parked] => :idling
      end

      event :gear_up do
        transition  [:idling] => :first_gear,
                    [:first_gear] => :second_gear,
                    [:second_gear] => :third_gear,
                    [:third_gear] => :crashed
      end
      event :gear_down do
        transition  [:first_gear] => :idling,
                    [:second_gear] => :first_gear,
                    [:third_gear] => :second_gear
      end

      event :crash do
        transition  [:first_gear, :second_gear, :third_gear] => :crashed
      end

      event :repair do
        transition  [:crashed] => :parked
      end
    end
end

vehicleA = Popvehicle.new
vehicleB = Popvehicle.new

while true do
  puts vehicleA.state
  puts " vehicle A can do #{vehicleA.state_events}"
  input = ask"actions"
  vehicleA.send(input)

  puts vehicleB.state
  puts " vehicle B can do #{vehicleB.state_events}"
  input = ask"actions"
  vehicleB.send(input)
end
